#language: pt

Funcionalidade: Get Make

              Eu como cliente da api get /api/OnlineChallenge/Make
              Quero realizar uma requisição 
              Para receber a lista de marcas cadastradas na webmotors

        Cenário: Listar Marcas
            Dado o end-point "https://desafioonline.webmotors.com.br/api/OnlineChallenge/Make"
             Quando eu realizar uma requisição do tipo GET
             Então o sistema deverar retornar o status code "200"
              E o content type deverá ser igual a "application/json"
              E a lista retornada não deverá ser vazia
              E a estrutura da lista retornada deverá ser conforme definida no contrato "schema"

