#language: pt

Funcionalidade: get Model

              Eu como cliente da api get /api/OnlineChallenge/Model
              Quero realizar uma requisição
              Para retornar os modelos de acordo com a marca informada

        Cenário: Listar modelos - sucesso
            Dado o end-point "https://desafioonline.webmotors.com.br/api/OnlineChallenge/Model"
              E o parametro MakeID for igual a "1"
             Quando eu realizar uma requisição do tipo GET
             Então o sistema deverar retornar o status code "200"
              E o content type deverá ser igual a "application/json"
              E a lista retornada não deverá ser vazia
              E a estrutura da lista retornada deverá ser conforme definida no contrato "schema"

        Cenário: Listar modelos - lista vazia
            Dado o end-point "https://desafioonline.webmotors.com.br/api/OnlineChallenge/Model"
              E o parametro MakeID for igual a "9999999"
             Quando eu realizar uma requisição do tipo GET
             Então o sistema deverar retornar o status code "200"
              E o content type deverá ser igual a "application/json"
              E a lista retornada deverá ser vazia