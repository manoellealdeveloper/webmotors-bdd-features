#language: pt

Funcionalidade: Get Vehicles

              Eu como cliente da api get /api/OnlineChallenge/Vehicles
              Quero realizar uma requisição
              Para retornar os veículos de acordo com a página informada

        Cenário: Listar veículos - sucesso
            Dado o end-point "https://desafioonline.webmotors.com.br/api/OnlineChallenge/Vehicles"
              E o parametro Page for igual a "1"
             Quando eu realizar uma requisição do tipo GET
             Então o sistema deverar retornar o status code "200"
              E o content type deverá ser igual a "application/json"
              E a lista retornada não deverá ser vazia
              E a estrutura da lista retornada deverá ser conforme definida no contrato "schema"

        Cenário: Listar veículos - lista vazia
            Dado o end-point "https://desafioonline.webmotors.com.br/api/OnlineChallenge/Vehicles"
              E o parametro Page for igual a "999999"
             Quando eu realizar uma requisição do tipo GET
             Então o sistema deverar retornar o status code "200"
              E o content type deverá ser igual a "application/json"
              E a lista retornada deverá ser vazia