#language: pt

Funcionalidade: Get Version

              Eu como cliente da api get /api/OnlineChallenge/Version
              Quero realizar uma requisição
              Para retornar as versões de acordo com a marca informada

        Cenário: Listar versões - sucesso
            Dado o end-point "https://desafioonline.webmotors.com.br/api/OnlineChallenge/Version"
              E o parametro MakeID for igual a "1"
             Quando eu realizar uma requisição do tipo GET
             Então o sistema deverar retornar o status code "200"
              E o content type deverá ser igual a "application/json"
              E a lista retornada não deverá ser vazia
              E a estrutura da lista retornada deverá ser conforme definida no contrato "schema"

        Cenário: Listar versões - lista vazia
            Dado o end-point "https://desafioonline.webmotors.com.br/api/OnlineChallenge/Version"
              E o parametro MakeID for igual a "9999999"
             Quando eu realizar uma requisição do tipo GET
             Então o sistema deverar retornar o status code "200"
              E o content type deverá ser igual a "application/json"
              E a lista retornada deverá ser vazia