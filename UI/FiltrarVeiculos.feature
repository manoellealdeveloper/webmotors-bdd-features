
#language: pt
@Filtrar-veiculos
Funcionalidade: Filtrar veiculos

  Eu como um usuário do webmotors
  Quero preencher o filtro de pesquisa disponibilizado
  Para que eu possa visualizar veículos de acordo com o valor digitado

  Contexto:
    Dado que eu acesse a url "https://hportal.webmotors.com.br"

  Cenário: Realizar pesquisa na home
    Dado informo no campo filtro de pesquisa o valor "Honda City"
    Quando eu selecionar a opção "Honda City"
    Então o sistema deverá retorna uma lista de veículos "HONDA CITY"

  Cenário: Realizar pesquisa na home
    Quando informo no campo filtro de pesquisa o valor "teste"
    Então o sistema deverá apresentar a mensagem "Não encontramos este termo, verifique a ortografia"