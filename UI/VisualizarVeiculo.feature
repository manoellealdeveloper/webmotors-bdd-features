#language: pt
@Visualizar-veiculo

Funcionalidade: Visualizar veículo

  Eu como cliente do webmotors
  Quero pesquisar veículos na plataforma
  Para encontrar veículos que atendam a minha necessidade

  Contexto:
    Dado que eu acesse a url "https://hportal.webmotors.com.br"
    E clico na opção Ver ofertas
    Quando a lista de veículos for apresentada


  Cenário: Visualizar veículo
    Quando eu selecionar um veículo para visualização
    Então o sistema deverá apresentar a tela de detalhamento do veículo selecionado

  Cenário: Enviar mensagem ao vendedor
    Quando eu selecionar um veículo para visualização
    E preencher os campos de mensagem de vendedor
      | nome     | pessoa teste    |
      | email    | teste@teste.com |
      | telefone | 89996633225     |
      | mensagem | mensagem teste  |
    E submeter a mensagem
    Então o sistema deverá apresentar a mensagem "Mensagem enviada!"